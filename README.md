# **E**lectronic **T**ransport **i**n **C**rystalline and **O**rganic **S**emiconductors

_The official repository for the ETiCOS laboratories implementations._

## 🚀 Let's begin 🚀

_Keep a cleaner and useful repository for the improvement of the teamwork._


### 📋 Prerequisites 📋

- **MATLAB 2020b** 


### 📦 Folders 📦

- **in process.** 

## 🛠️ Software 🛠️

* [MATLAB 2020b](https://www.mathworks.com/)
* [Git](https://git-scm.com/) 

## ✒️ Autores ✒️

✒ **Sergio González Correa** - *Developer* - [scorrea](https://gitlab.com/scorrea)

✒ **Matteo Fiorillo** - *Developer* 

## 🎁 Gratefulness 🎁

* Thank you so much, electrons. 